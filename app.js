const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

const company = require('./routes/company')
const representative = require('./routes/representative')

const app = express()
const port = 5000

// Body Parser
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json()); 


// Connecting to DB 
mongoose
	.connect("mongodb://localhost:27017/node-assignment", { useNewUrlParser: true , useUnifiedTopology: true })
	.then(() => console.log('MongoDB Connected'))
	.catch(err => console.log(err));

// use routes at /api/company/*
app.use("/api/company", company)

// use routes at /api/representative/*
app.use("/api/representative", representative)


app.listen(port, () => {
  console.log(`Server is running at http://localhost:${port}`)
})