const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Create Schema
const companySchema = new Schema({

	name: {
		type: String,	
		required: true
	},
	address: {
		type: String,	
		required: true
	},
	state: {
		type: String,	
		required: true
	},  
	entityType: {
		type : String
	},  
	planId: {
		type : Number
	},
	representatives: {
		type: []
	}
});

module.exports = Company = mongoose.model('company', companySchema);