const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Create Schema
const representativeSchema = new Schema({

	name: {
		type: String,	
		required: true
	},
	phone: {
		type: String,	
		required: true
	}
});

module.exports = Representative = mongoose.model('representative', representativeSchema);