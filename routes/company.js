const express = require("express");
const router = express.Router();

const Company = require("../models/company");
const Representative = require("../models/representative");

// post api to /api/company/create
router.post("/create", function (req, res) {
  const name = req.body.name;
  const address = req.body.address;
  const state = req.body.state;
  const entityType = req.body.entityType;
  const planId = req.body.planId;
  const representatives = req.body.representatives;

  if (name !== "" && address !== "" && state !== "") {
    const newCompany = new Company({
      name,
      address,
      state,
      entityType,
      planId,
      representatives,
    });
    newCompany.save(function (err) {
      if (err) {
        res.status(500).json({ status: "error", message: "Invalid Data" });
        return;
      }
      res
        .status(200)
        .json({ state: "success", message: "Company added Successfully" });
    });
  } else {
    res.status(500).json({ status: "error", message: "Invalid Data" });
  }
});

// post api to /api/company/update
router.post("/update", function (req, res) {
  const _id = req.body._id;
  const name = req.body.name;
  const address = req.body.address;
  const state = req.body.state;
  const entityType = req.body.entityType;
  const planId = req.body.planId;
  const representatives = req.body.representatives;

  if (_id != "") {
    Company.findOneAndUpdate(
      { _id },
      {
        $set: {
          name,
          address,
          state,
          entityType,
          planId,
          representatives,
        },
      },
      { new: true }
    )
      .then((company) => {
        res.status(200).json({
          state: "success",
          message: "Company Updated Successfully",
          result: company,
        });
      })
      .catch(function (err) {
        res
          .status(500)
          .json({ state: "error", message: err.message, result: "" });
      });
  } else {
    res.status(500).json({ status: "error", message: "Invalid Data" });
  }
});

// post api to /api/company/get
router.post("/get", function (req, res) {
  const _id = req.body._id;

  if (_id != "") {
    Company.findOne({ _id })
      .then(async (company) => {
        let representatives = company.representatives;
        if (representatives && representatives.length >= 0) {
          representatives = await Representative.find({
            _id: { $in: representatives },
          });
        }

        res.status(200).json({
          state: "success",
          result: Object.assign(JSON.parse(JSON.stringify(company)), {
            representatives,
          }),
        });
      })
      .catch(function (err) {
        res.status(500).json({ state: "error", result: "" });
      });
  } else {
    res.status(500).json({ status: "error", message: "Invalid Data" });
  }
});

module.exports = router;
