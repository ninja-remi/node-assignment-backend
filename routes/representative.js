const express = require("express");
const router = express.Router();
 
const Representative = require('../models/representative')

router.post("/create", function(req, res){


    const name = req.body.name
    const phone = req.body.phone
     
    if(name!=="" && phone!==""){
        const newRepresentative = new Representative({
            name,
            phone
        });
        newRepresentative.save(function(err){
            if(err){
                res.status(500).json({status:'error', message:"Invalid Data"})
                return
            }
            res.status(200).json({state:'success', message:"Representative added Successfully"})
        })
    }else{
        res.status(500).json({status:'error', message:"Invalid Data"})
    }

   

});

module.exports = router;
